package com.example.demo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entity.User;
import com.example.demo.repository.UserRepository;

@Service
public class LoginService {

	@Autowired
	private UserRepository userRepository;
	
	public boolean authenticate(String email, String paswd) {
		try {
			User u = userRepository.findByEmail(email);
			if (u==null || !paswd.equals(u.getPaswd())) {
				return false;
			}
			
		}catch(Exception e) {
			e.printStackTrace();
		}
		return true;
	}
}
