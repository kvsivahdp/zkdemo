package com.example.demo.service;

import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entity.User;
import com.example.demo.repository.UserRepository;

@Service
public class UserService {
	
	@Autowired
	private UserRepository userRepository;
	
	public User saveUser(User user) {
		try {
			return userRepository.save(user);
		}catch(Exception e) {
			throw e;
		}
	}
	
	public List<User> searchUsers(String searchTerm){
		try {
			List<User> userList = userRepository.findAll();
			System.out.println("userList....size...."+userList);
			if (StringUtils.isEmpty(searchTerm)){
				return userList;
			}else{
				return userList.stream().filter(x->x.getEmail().toLowerCase().contains(searchTerm.toLowerCase()) 
						|| x.getName().toLowerCase().contains(searchTerm.toLowerCase()))
				.collect(Collectors.toList());
			}
		}catch(Exception e) {
			throw e;
		}
	}

}
