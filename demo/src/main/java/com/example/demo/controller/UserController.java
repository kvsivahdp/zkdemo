package com.example.demo.controller;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zkplus.spring.DelegatingVariableResolver;
import org.zkoss.zul.Label;
import org.zkoss.zul.Textbox;

import com.example.demo.entity.User;
import com.example.demo.service.UserService;

@VariableResolver(DelegatingVariableResolver.class)
public class UserController extends SelectorComposer<Component> {

	private static final long serialVersionUID = 1L;

	@WireVariable
	private UserService userService;

	@Wire
	private Textbox name;

	@Wire
	private Textbox email;

	@Wire
	private Textbox uname;

	@Wire
	private Textbox paswd;

	@Wire
	private Textbox repaswd;

	@Wire
	private Label message;

	@Override
	public void doAfterCompose(Component comp) throws Exception {
		super.doAfterCompose(comp);
	}

	@Listen("onClick=#registerBtn; onOK=#registerBtn")
	public void register() {
		User newUser = new User();
		newUser.setName(name.getValue());
		newUser.setEmail(email.getValue());
		newUser.setPaswd(paswd.getValue());
		if (!repaswd.getValue().equals(paswd.getValue())) {
			message.setValue("Password and confirm password does not match");
			message.setZclass("warn");
			return;
		}
		userService.saveUser(newUser);
		name.setRawValue("");
		email.setRawValue("");
		paswd.setRawValue("");
		repaswd.setRawValue("");
		message.setValue("User Registered ");
		message.setZclass("warn");
	}
	@Listen("onClick=#login")
	public void logout() {
		Executions.sendRedirect("login");
	}
}
