package com.example.demo.controller;

import java.util.List;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zkplus.spring.DelegatingVariableResolver;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Textbox;

import com.example.demo.entity.User;
import com.example.demo.service.UserService;

@VariableResolver(DelegatingVariableResolver.class)
public class DisplayUserController extends SelectorComposer<Component> {
	@WireVariable
	private UserService userService;
	
	@Wire
	private Textbox searchTerm;
	
	@Wire
	private Listbox userListbox;
	
	@Override
	public void doAfterCompose(Component comp) throws Exception {
		super.doAfterCompose(comp);
	}
	
	@Listen("onClick = #searchBtn")
    public void search(){
        String searchStr = searchTerm.getValue();
        List<User> userList = userService.searchUsers(searchStr);
        userListbox.setModel(new ListModelList<User>(userList));
    }
	
	@Listen("onClick = #logout")
	public void logout() {
		Executions.sendRedirect("login");
	}

}
