package com.example.demo.controller;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zkplus.spring.DelegatingVariableResolver;
import org.zkoss.zul.Label;
import org.zkoss.zul.Textbox;

import com.example.demo.service.LoginService;

@VariableResolver(DelegatingVariableResolver.class)
public class LoginController extends SelectorComposer<Component> {
	
	private static final long serialVersionUID = 1L;
	
	@WireVariable
	private LoginService loginService;
	
	@Wire
	Textbox email;
	@Wire
	Textbox paswd;
	@Wire
	Label message;
	
	@Override
	public void doAfterCompose(Component comp) throws Exception {
		super.doAfterCompose(comp);
	}
	
	@Listen("onClick=#login; onOK=#loginWin")
	public void authLogin(){
		boolean authenticated = loginService.authenticate(email.getValue(), paswd.getValue());
		if(!authenticated) {
			message.setValue("Email or Password incorrect");
			message.setZclass("warn");
			return;
		}
		Executions.sendRedirect("home");
	}
	
	@Listen("onClick=#register")
	public void navigateRegister(){
		Executions.sendRedirect("register");
	}

}
