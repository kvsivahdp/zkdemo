package com.example.demo.controller;

import org.jfree.text.TextBox;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zkplus.spring.DelegatingVariableResolver;
import org.zkoss.zul.Borderlayout;
import org.zkoss.zul.Center;
import org.zkoss.zul.Include;
import org.zkoss.zul.Menubar;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Window;

@VariableResolver(DelegatingVariableResolver.class)
public class HomeController extends SelectorComposer<Component> {

	private static final long serialVersionUID = 1L;
	@Wire
	Center mainContent;

	@Wire
	Borderlayout mainBorder;

	@Wire
	Menuitem contatti;

	@Wire
	Include mainContentInclude;

	@Wire
	Menubar menubar;
	
	@Wire
	TextBox testval;
	
	@Wire
	Window addUserBlock;
	
	@Wire
	Window editUserBlock;
	
	@Wire
	Window listUserBlock;
	
	@Override
	public void doAfterCompose(Component comp) throws Exception {
		super.doAfterCompose(comp);
	}
	@Listen("onClick = #menubar > menu > menupopup > menuitem")
	public void change_page(Event e){
	    String menuItemId = e.getTarget().getId();
	    if("addUser".equals(menuItemId)) {
	    	addUserBlock.setVisible(true);
	    	editUserBlock.setVisible(false);
	    	listUserBlock.setVisible(false);
	    } else if("editUser".equals(menuItemId)) {
	    	addUserBlock.setVisible(false);
	    	editUserBlock.setVisible(true);
	    	listUserBlock.setVisible(false);
	    }else if("listUser".equals(menuItemId)) {
	    	listUserBlock.setVisible(true);
	    	addUserBlock.setVisible(false);
	    	editUserBlock.setVisible(false);
	    }else if("logoutUser".equals(menuItemId)) {
	    	Executions.sendRedirect("login");
	    }
	    
	}
}
