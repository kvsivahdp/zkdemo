package com.example.demo.controller;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zkplus.spring.DelegatingVariableResolver;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModel;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Textbox;

import com.example.demo.entity.User;
import com.example.demo.service.UserService;

@VariableResolver(DelegatingVariableResolver.class)
public class EditUserController extends SelectorComposer<Component> {

	private static final long serialVersionUID = 1L;

	@WireVariable
	private UserService userService;

	@Wire
	private Textbox name;

	@Wire
	private Textbox email;

	
	@Wire
	private Label message;
	
	private ListModel<User> userModel = new  ListModelList<User>();
	
	@Wire
	private Listbox userSelectbox;
	
	public EditUserController(){
	}
	
	@Override
	public void doAfterCompose(Component comp) throws Exception {
		super.doAfterCompose(comp);
		userModel = new  ListModelList<User>(userService.searchUsers(""));
		userSelectbox.setModel(userModel);
	}
	
	public ListModel<User> getUserModel() {
        return userModel;
    }
	
	@Listen("onSelect=#userSelectbox")
	public void edit() {
		System.out.println("selected value:"+userSelectbox.getSelectedItem().getValue());
		User selectedUser = userSelectbox.getSelectedItem().getValue();
		name.setValue(selectedUser.getName());
		email.setValue(selectedUser.getEmail());
		
		//userSelectbox.setModel((ListModel<?>) userService.searchUsers(""));
		/*User newUser = new User();
		newUser.setName(name.getValue());
		newUser.setEmail(email.getValue());
		newUser.setPaswd(paswd.getValue());
		if (!repaswd.getValue().equals(paswd.getValue())) {
			message.setValue("Password and confirm password does not match");
			message.setZclass("warn");
			return;
		}
		userService.saveUser(newUser);
		name.setRawValue("");
		email.setRawValue("");
		paswd.setRawValue("");
		repaswd.setRawValue("");
		message.setValue("User Registered ");
		message.setZclass("warn");*/
	}
	@Listen("onClick=#login")
	public void logout() {
		Executions.sendRedirect("login");
	}
}
